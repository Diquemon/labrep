///////////////////////////////////////
var isRightAns = function(a, b){
  if(a==b) return true;
  return false;
}
Template.testresult.helpers({
  chTask: function(){
      var n = new Array();
      var j = 0;
      var t = testZNO.find().fetch();
      var testanswrs = testZNO.find({}, {fields: {subject: 1, answr:1 , points: 1}}).fetch();
      var tmp = Session.get("answ");
      for (var i = 0; i < t.length; i++) {
        j=i+1;
        if (i==Number(Session.get("currTask")-1))
                            {
        n[i] = "<p class=\"active-test current-ans\" data-num="+j.toString()+" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
        Session.set("testnow", t[i]._id);
        Session.set("typ", t[i].type);
                            }
      else {
        if(isRightAns(tmp[i], testanswrs[i].answr)){
        n[i] = "<p id=\"poss\" class=\"active-test correct-ans\" data-num="+j.toString()+" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
            }
        else  {
          n[i] = "<p id=\"poss\" class=\"active-test ready-ans\" data-num="+j.toString()+" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
              }
          }

                                        }
        return n;
    },
  checkResult: function(){
    var jndx = Session.get("currTask")-1;
    var indx = Session.get("taskIds")[jndx];
    var trueAns = testZNO.find({}, {fields: {subject: 1, answr:1 , points: 1}}).fetch();

    if(trueAns == this){
    return true;}
    return false;
  },

  Variants: function(){return testZNO.findOne({_id: Session.get("testnow")}, {fields: {variants: 1}}).variants;},
  Year: function(){return Session.get("year"); },
  Session: function(){return Session.get("ses");},
  Subject: function(){return Lessons.findOne({_id: Session.get("subj")}, {fields: {name: 1}}).name;},
  Text: function(){return testZNO.findOne({_id: Session.get("testnow")}, {fields: {text: 1}}).text;},
  comment: function(){ return testZNO.findOne({_id: Session.get("testnow")}, {fields: {comment: 1}}).comment; },

  checkOnCurr: function(){
    var jndx = Session.get("currTask")-1;
    var currAns = Session.get("answ")[jndx];
    if(this == currAns){
      return true;
    }
    return false;},
    currIsAnsw: function(){
      var jndx = Session.get("currTask")-1;
      var currAns = Session.get("answ")[jndx];
      if(testZNO.findOne({_id: Session.get("testnow")}, {fields: {text: 1}}).answr == currAns){
        return true;
      }
      return false;
    },
    thisIsAnsw: function(){
      if(testZNO.findOne({_id: Session.get("testnow")}, {fields: {text: 1}}).answr == this){
        return true;
      }
      return false;
    },
    typeCheck: function(val){console.log("VALUE="+val);
      if (val == Session.get("typ")) {
        console.log("rueEEEE"); return true;
      }
      return true;
    },
    getCountdown: function(){
    var _time = Number(cntdn.get());
    if(Session.get("currTime")){_time=Session.get("currTime");}
    Session.set();
      var hours = _time/3600^0;
      var mins = (_time-hours*3600)/60^0;
      var sec = _time-hours*3600-mins*60;

      return ((hours<10?"0"+hours:hours)+":"+(mins<10?"0"+mins:mins)+":"+(sec<10?"0"+sec:sec));
    }

});

Template.testresult.events({
  "click .nexta": function(event){
//  event.stopPropagation()
    event.preventDefault();
        if(Number($('.test-now').text())!=30){
          Session.set("currTask", (Number(Session.get("currTask"))+1));//.getAttribute("data-ch"));
          Session.set("testnow", $('.test-now').next().attr("data-ch"));
        }
        else{
          Session.set("currTask", 1);//.getAttribute("data-ch"));
          Session.set("testnow", Number($('[data-num=0]').text()));
        }
    },
    "click .lose-text": function(event){
      Router.go("/statistic/");
    },
  "click #poss": function(event){
  //  event.stopPropagation();
      event.preventDefault();
    console.log($(event.currentTarget).text());
    Session.set("currTask", $(event.currentTarget).text());
    Session.set("testnow", event.currentTarget.getAttribute("data-ch"));

  }



});
