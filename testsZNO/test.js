Template.test.helpers({
	
	
  chTask: function(){
    var n = new Array();
    var j = 0;

    var t = testZNO.find().fetch();//Session.get("taskIds"); //array of ids
    console.log("NUMS: "+t);
    console.log("test: "+t);
    for (var i = 0; i < t.length; i++) {
      j=i+1;
      console.log("CURR="+Session.get("currTask"));
      if (i==Number(Session.get("currTask")-1)) {
      n[i] = "<p class=\"test-now\" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
      Session.set("testnow", t[i]._id);
      Session.set("typ", t[i].type);
    }
    else {
      n[i] = "<p id=\"poss\" class=\"active-test\" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
    }

  }
    return n;
  },
  Variants: function(){return testZNO.findOne({_id: Session.get("testnow")}, {fields: {variants: 1}}).variants;},
  Year: function(){return Session.get("year"); },
  Session: function(){return Session.get("ses");},
  Subject: function(){return Lessons.findOne({_id: Session.get("subj")}, {fields: {name: 1}}).name;},
  Text: function(){return testZNO.findOne({_id: Session.get("testnow")}, {fields: {text: 1}}).text;},

  typeCheck: function(val){console.log("VALUE="+val);
    if (val == Session.get("typ")) {
      console.log("rueEEEE"); return true;
    }
    return true;
  },
checkFinish: function(){
  if (!Session.get("answ")){return false;}
  var temp = Session.get("answ");
  for (var i = 0; i < temp.length; i++) {
    if(temp[i]==null) {
      return false;
    }
  }
  return true;
},
getCountdown: function(){
var _time = Number(cntdn.get());
if(Session.get("currTime")){_time=Session.get("currTime");}
Session.set();
  var hours = _time/3600^0;
  var mins = (_time-hours*3600)/60^0;
  var sec = _time-hours*3600-mins*60;

  return ((hours<10?"0"+hours:hours)+":"+(mins<10?"0"+mins:mins)+":"+(sec<10?"0"+sec:sec));
},
pauser: function(){
  if(Session.get("ispause")==true)
  return true;
  return false;
}

});

Template.test.events({

  "click #poss": function(event){
    Session.set("currTask", $(event.currentTarget).text());//.getAttribute("data-ch"));
    Session.set("testnow", event.currentTarget.getAttribute("data-ch"));
    var tmp;

    if (Session.get("answ")) {
      tmp = Session.get("answ");
      console.log( $('.qwe').val());
      tmp[Session.get("currTask")-1] = $('.qwe').val();//document.getElementsByClassName("ch-test")[0].getElementsByClassName("ewq")[0].getAttribute("data-ans");
      Session.set("answ", tmp);
    } //p
    else {
      tmp = new Array(30);
      console.log(document.getElementsByClassName("ch-test")[0]);
      if($('.qwe').val()){
      //  var tt = testsZNO.findOne({_id: this.params._id}, {Variants: 1});
      console.log( $('.qwe').val());

      tmp[Session.get("currTask")-1] = $('.qwe').val();}  //document.getElementsByClassName("ch-test")[0].getElementsByClassName("ewq")[0].getAttribute("data-ans");"}"
      Session.set("answ", tmp);
    }
    //add to Session answers from Task!!!
  },
  "click .ewq": function(event){
        $('.qwe').attr('class', 'ewq');
       event.currentTarget.setAttribute('class', 'qwe'); //
    },
"click #buttNext": function(event){
      var tmp;

      if (Session.get("answ")) {
        tmp = Session.get("answ");
        console.log( $('.qwe').val());
        tmp[Session.get("currTask")-1] = $('.qwe').val();
        Session.set("answ", tmp);
      }
      else {
        tmp = new Array(30);
        //console.log("AAAA"+document.getElementsByClassName("ch-test")[0]);

        if($('.qwe').val()){
        //  var tt = testsZNO.findOne({_id: this.params._id}, {Variants: 1});
        console.log( $('.qwe').val());

        tmp[Session.get("currTask")-1] = $('.qwe').val();}
        Session.set("answ", tmp);
      }
      if(Number($('.test-now').text())!=30){
        Session.set("currTask", (Number(Session.get("currTask"))+1));//.getAttribute("data-ch"));
        console.log($('.test-now').next().attr("data-ch"));
        Session.set("testnow", $('.test-now').next().attr("data-ch"));
        var d = new Date();
        var mon = d.getMonth()+1;
        Session.set("date:", (d.getDate()+ "."+mon+"."+d.getFullYear()));
      }


  },
  "click #finish": function(event){
    //cntdn.defaultset();
    var temp = Number(cntdn.get());
    cntdn.set();
    var success = false;
    Session.set("currTime", temp);
    var testanswrs = testZNO.find({}, {fields: {subject: 1, answr:1 , points: 1}}).fetch();
    var tmp = Session.get("answ");
    var b = 0;
    var pts = 0;
    for(var i=0; i<tmp.length; i++){

      if(tmp[i]==testanswrs[i].answr) {

        b++;
      pts=pts+Number(testanswrs[i].points);
      }
    }
    Session.set("b", b); Session.set("points", pts);
    if (Meteor.userId()) {
      Meteor.call("journalZNO", pts, testanswrs[0].subject, Meteor.userId());
      if(pts>120){success=true;} //(uid, subj, points, success, compl)
      Meteor.call("updateResults", Meteor.userId(), testanswrs[0].subject, pts, success, true);
    }

      //delete Session.key.test-now;
    Router.go("/testdone/");
  },
  "click #lose": function(event){
      $('.pop-up').attr("style", "display: block;");
  },
  "click .close-btn": function(event){
      $('.pop-up').attr("style", "display: none;");
  },
  "click .losebtn": function(event){
  
    if (Meteor.userId()) {
      Meteor.call("jZNOFail", Session.get("subj"), Meteor.userId());
      Meteor.call("updateResults", Meteor.userId(), Session.get("subj"), 0, false, false);
    }
    Router.go('/statistic/');
  },
  "click .pause": function(event){
    if(Session.get("ispause")){
      Session.set("ispause", false);
      cntdn.start();
      delete Session.keys.currTime;

}
else{
    Session.set("ispause", true);
    var temp = Number(cntdn.get());
    cntdn.set();
    Session.set("currTime", temp);}
}
});
    //window.countdown.stop();
//  Session.set("timeForRemove", (7200-window.countdown.get()));}





Template.test.onDestroyed(function () {
  if(Session.get("journal")=="completed"){}
  else{
  var d = new Date();
  var mon = d.getMonth()+1;
  if(Meteor.userId()){
    Meteor.users.update({_id:idSelector}, {$addToSet:{
        journal: []
    }});

  }
}
});
