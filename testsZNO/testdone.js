Template.testdone.helpers({

  Year: function(){return Session.get("year"); },
  Session: function(){return Session.get("ses");},
  Subject: function(){return Lessons.findOne({_id: Session.get("subj")}, {fields: {name: 1}}).name;},
  numberOfRightAnswrs: function(){return Session.get("b");},
  testPoints: function(){return Session.get("points");},
  creativePoints: function(){ var cp = Session.get("cpoints")==null?"0":Session.get("cpoints"); return cp;},
  getCountdown: function(){
  var _time=0;
  if(Session.get("currTime")){_time=Session.get("currTime");}
    var hours = _time/3600^0;
    var mins = (_time-hours*3600)/60^0;
    var sec = _time-hours*3600-mins*60;
    return ((hours<10?"0"+hours:hours)+":"+(mins<10?"0"+mins:mins)+":"+(sec<10?"0"+sec:sec));
  },
  chTask: function(){
    var n = new Array();
    var j = 0;
    var t = testZNO.find().fetch();
    var testanswrs = testZNO.find({}, {fields: {subject: 1, answr:1 , points: 1}}).fetch();
    var tmp = Session.get("answ");
    for (var i = 0; i < t.length; i++) {
      j=i+1;
      if (i==Number(Session.get("currTask")-1))
                          {
      n[i] = "<p class=\"active-test current-ans\" data-ch=\""+t[i]._id+"\">"+j.toString()+"</p>";
      Session.set("testnow", t[i]._id);
      Session.set("typ", t[i].type);
                          }
    else {
      if(isRightAns(tmp[i], testanswrs[i].answr)){
      n[i] = "<p id=\"poss\" class=\"active-test correct-ans\" href=\"{{pathFor 'test' _id="+t[i]+"}}\" data-ch=\""+j.toString()+"\">"+j.toString()+"</p>";
          }
      else  {
        n[i] = "<p id=\"poss\" class=\"active-test ready-ans\" href=\"{{pathFor 'test' _id="+t[i]+"}}\" data-ch=\""+j.toString()+"\">"+j.toString()+"</p>";
            }
        }

                                      }
      return n;
  },
  percentsResult: function(){
    var percent = Number(Session.get("b"))*100/30;
    return percent;
  }
});
var isRightAns = function(a, b){
  if(a==b) return true;
  return false;
}
Template.testdone.events({
  "click .res-but": function(){
  //  event.stopPropagation();
        event.preventDefault();
    Router.go("/testresult/");
  },
  "click lose-text": function(){
    Router.go("/statistic/");
  }
});
